# _Loot of Lima_

_Loot of Lima_ is a deduction table top game for 1 to 5 players designed by Lary Levy and published by [BoardGameTables.com](https://www.boardgametables.com/products/loot-of-lima). Players ask coded questions regarding the location of a secret treasure using a compass coordinate system, with the objective of trying to deduce the location of the treasure before the other players.

# This Companion App

This companion app is free and open source software licensed under the AGPLv3, and is intended as a digital supplement for the 1 and 2 player variants of the game. The author(s) of this app are not affiliated with BoardGameTables.com.

It is accessible at [https://LootofLima.vtkellers.com](https://LootofLima.vtkellers.com) but a self-hosted option is also available (see below).

# Screenshots

<p><img src="img/setup.png" height="600px"></p>
<p><img src="img/question.png" height="600px"></p>
<p><img src="img/victory.png" height="600px"></p>

# Self-Hosting

Advanced users may self-host the _Loot of Lima_ Companion App on their own computer hardware using these steps:

1. [Download and install R](https://www.r-project.org/).
2. Clone this repository:

    ```
    git clone https://gitlab.com/jeff.keller/lootoflimacompanion.git
    ```
    
3. From inside the `lootoflimacompanion` directory, install the app dependencies:

    ```
    Rscript install_dependencies.R
    ```

4. Run the app with:

    ```
    R -e "shiny::runApp()"
    ```
    
    This command will print an address--visit it in your browser. For example: `http://127.0.0.1:5948`.
    
    Use the `host` and `port` arguments to customize your deployment. For example:
    
    ```
    R -e "shiny::runApp(host = '0.0.0.0', port = 12345)"
    ```
